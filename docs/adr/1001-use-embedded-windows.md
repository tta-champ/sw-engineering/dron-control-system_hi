#1001. Use Embedded Windows

Date: 2022-04-26

## Status

Accepted

## Context

ATM 장비의 메인 OS로 기존 개발된 코드가 Windows기반으로 되어있다.
개발자도 Windows 개발에 능숙해서 별도의 학습 시간이 필요업다.


##Decision

현재 최신 버전인 Windows11 보다는 안정적이라고 평가받는 10 버전으로 OS로 선정한다.

##Consequences

기존 Windows 7로 개발된 코드를 10에서 잘 되는지 확인하고, deprecated 된 라이브러리는 최신화시켜 검증한다.
